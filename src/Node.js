import React, {Component} from 'react';
import './Node.css'

class Node extends Component {
    constructor(props){
        super(props);
        this.style = {
            left: this.props.x,
            top: this.props.y,
        }
    }   

    render () {
        return(
            <div className="Node" style={
                {
                    left: this.style.left + 'px',
                    top: this.style.top + 'px' 
                }
            }>New</div>
        )
    }
}

export default Node;