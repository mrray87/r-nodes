import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import New from './New';
import './index.css';

ReactDOM.render(
  <App />,
  document.getElementById('root')
);

ReactDOM.render(
  <New />,
  document.getElementById('title')
)