import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

import Node from'./Node';

class App extends Component {
  constructor() {
    super();
    this.state = {
      nodes: []
    };
  }

  createNode(e) {
    const n = this.state.nodes;
    n.push(
      { x: e.nativeEvent.offsetX, y: e.nativeEvent.offsetY}
    )
    console.log(n)
    this.setState({
      nodes: n
    })
  }

  render() {
    return (
      <div className="App">
        <div className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h2>Welcome to React</h2>
        </div>
        <div className="Main" onClick={this.createNode.bind(this)}>
          {this.state.nodes.map((item, _) => {
            return(<Node x={item.x} y={item.y} />)
          })}
        <Node x="50" y="400" />
        </div>
      </div>
    );
  }
}

export default App;
